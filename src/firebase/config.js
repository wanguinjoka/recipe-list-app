import  firebase from 'firebase/app'
import 'firebase/firestore'


const firebaseConfig = {
    apiKey: "AIzaSyBvsE7w_BNOp5ao8RCF0zLuV9qkIQngDwQ",
    authDomain: "cook-book-274d2.firebaseapp.com",
    projectId: "cook-book-274d2",
    storageBucket: "cook-book-274d2.appspot.com",
    messagingSenderId: "229569003612",
    appId: "1:229569003612:web:0a4408caf323350e2a6699"
  };

//   init firebase
firebase.initializeApp(firebaseConfig)

// init services
const projectFirestore = firebase.firestore()

export{ projectFirestore }