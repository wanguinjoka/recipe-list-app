import './Recipe.css'
import React from 'react';

// to access the url ....recipe/id
import { useParams } from 'react-router-dom';
// import { useFetch } from '../../hooks/useFetch';
import { useTheme } from '../../hooks/useTheme';
import { useEffect, useState} from 'react';
import { projectFirestore } from '../../firebase/config';


export default function Recipe() {
  const { mode } = useTheme()
  const { id } = useParams()
  // const url = 'http://localhost:3000/recipes/'+ id
  // const { data, isPending, error } = useFetch(url)

  const [data, setData] = useState(null)
  const [isPending, setIsPending] = useState(false)
  const [error, setError] = useState(null)

  useEffect(() => {
    setIsPending(true)
    const unsub = projectFirestore.collection('recipes')
    // to get single doc
      .doc(id)
      // .get()
      // .then((doc) => {
      .onSnapshot((doc) => {
        if(doc.exists){
          setIsPending(false)
          setData(doc.data())
        }else{
          setIsPending(false)
          setError('Could not find recipe')
        }
      })
      return () => unsub()
      

  }, [id])

  const handleClick = () => {
    projectFirestore.collection('recipes')
        .doc(id)
        .update({
              title: ''
        })


  }


  return (
  <div className={`recipe ${mode}`}>
    {error && <p className='error'>{error}</p>}
    {isPending && <p className='loading'>Loading....</p>}
    {data && (
      <>
    <h2 className='page-title'>{data.title}</h2>
        <p>Takes{data.cookingTime} to cook</p>
    <ul>
      {data.ingredients.map(ing => <li key={ing}>{ing}</li>)}
    </ul>
    <p className="method">{data.method}</p>
   
    <button onClick={handleClick}> Update me</button>
    </>
    )}

  </div>);

}

