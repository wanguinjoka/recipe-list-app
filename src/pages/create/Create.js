import './Create.css'
import { useState, useRef , useEffect} from 'react'
// import { useFetch } from '../../hooks/useFetch';
import { projectFirestore } from '../../firebase/config';

import React from 'react';
import { useHistory } from 'react-router-dom';

export default function Create() {
  const [title, setTitle] = useState('')
  const [method, setMethod] = useState('')
  const [cookingTime, setCookingTime] = useState('')
  const[ newIng, setNewIng] = useState('')
  const [ingredients, setIngredients] = useState([])
  const ingredientsInput = useRef(null)
  const history = useHistory()

  // const { postData, data,error} = useFetch('http://localhost:3000/recipes', 'POST')

  const handleSubmit = async (e) => {
    e.preventDefault()
    const doc = {title,ingredients,method,cookingTime: cookingTime + 'minutes'}

    try{
       await projectFirestore.collection('recipes')
                .add(doc)
                history.push('/')
    }catch(err){
      console.log(err)
    }
  }

  const handleAdd= (e) =>{
    e.preventDefault()
    const ing = newIng.trim()
    if(ing && !ingredients.includes(ing)){
      setIngredients(prevIngredients=> [...prevIngredients, ing])
    }
    setNewIng('')
    ingredientsInput.current.focus()
  }
//  using useFetch
  // useEffect(()=>{
  //   if(data){
  //       history.push('/') 
  //   }
  // },[data])

  return (
  <div className='create'>
    <h2 className='page-title'>Add a New Recipe</h2>
    <form onSubmit={handleSubmit}>
      <label>
        <span>Recipe title:</span>
        <input 
           type='text'
           onChange={(e) => setTitle(e.target.value)}
           value ={title}
           required
        ></input>
      </label>
       <label>
         <span>Recipe ingredients:</span>
         <div className='ingredients'>
           <input type='text' 
               onChange={(e)=> setNewIng(e.target.value)}
               value={newIng}
               ref={ingredientsInput} />
           <button onClick={handleAdd} className='btn'>add</button>
         </div>
       </label>
       <p>Current ingredients: {ingredients.map(i => <em key={i}>{i}, </em>)}</p>

      <label>
        <span>Recipe method:</span>
        <textarea
            onChange={(e)=> setMethod(e.target.value)}
            value={method}
            required
        ></textarea>
      </label>
      <label>
        <span>
          Cooking Time (minutes):
        </span>
        <input 
          type='number'
          onChange={e =>setCookingTime(e.target.value)}
          value={cookingTime}
          required
          />
      </label>
      <button>Submit</button>
    </form>
  </div>
  );
}
