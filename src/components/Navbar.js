import './Navbar.css'
import { Link } from 'react-router-dom'


import React from 'react';
import SearchBar from './SearchBar';
import { useTheme } from '../hooks/useTheme';


export default function Navbar() {
   const { color, changeColor } = useTheme()


  return <div className='navbar' style={{ background: color}}>
      <nav>
          <Link to="/" className='brand'><h1>Cook Book</h1></Link>
          <SearchBar />
          <Link to="/create"><h2>Create</h2></Link>
      </nav>
  </div>;
}
